# version 330 core
// Do not use any version older than 330!

/* This is the fragment shader for reading in a scene description, including 
   lighting.  Uniform lights are specified from the main program, and used in 
   the shader.  As well as the material parameters of the object.  */

// Inputs to the fragment shader are the outputs of the same name of the vertex shader.
// Note that the default output, gl_Position, is inaccessible!
in vec3 mynormal; 
in vec4 myvertex; 

// You will certainly need this matrix for your lighting calculations
uniform mat4 modelview;

// This first defined output of type vec4 will be the fragment color
out vec4 fragColor;

uniform vec3 color;

const int numLights = 10; 
uniform bool enablelighting; // are we lighting at all (global).
uniform vec4 lightposn[numLights]; // positions of lights 
uniform vec4 lightcolor[numLights]; // colors of lights
uniform int numused;               // number of lights used

// Now, set the material parameters.
// I use ambient, diffuse, specular, shininess. 
// But, the ambient is just additive and doesn't multiply the lights.  

uniform vec4 ambient; 
uniform vec4 diffuse; 
uniform vec4 specular; 
uniform vec4 emission; 
uniform float shininess; 

vec4 ComputeLight (vec3 direction, vec4 lightcolor, vec3 normal, 
	vec3 halfvec, vec4 mydiffuse, vec4 myspecular, float myshininess) {

        float nDotL = dot(normal, direction)  ;         
        vec4 lambert = mydiffuse * lightcolor * max (nDotL, 0.0) ;  

        float nDotH = dot(normal, halfvec) ; 
        vec4 phong = myspecular * lightcolor * pow (max(nDotH, 0.0), myshininess) ; 

        vec4 retval = lambert + phong ; 
        return retval ;
}

void main (void) 
{       
    if (enablelighting) {       
        vec4 finalcolor; 

        // YOUR CODE FOR HW 2 HERE
        // A key part is implementation of the fragment shader

		/* transform mynormal & myvertex with modelview matrix */
		vec4 _myvertex = modelview * myvertex;
		//vec3 eye_mynormal = (modelview * vec4(mynormal, 0.0)).xyz;
		vec3 _mynormal = mat3(transpose(inverse(modelview))) * mynormal;


		/*turn back into vec3 types & dehomogenize 'myvertex' (current location)*/
		const vec3 eyepos3 = vec3(0,0,0);
		vec3 mypos3 = _myvertex.xyz/_myvertex.w;

		//create 'eye direction', b/c half-vector = normalize(lightpos+eyedir)
		vec3 eyedirn3 = normalize(eyepos3 - mypos3);
		vec3 normal3 = normalize(_mynormal);
		
		/* Implement formula:
			I (finalcolor) = 
				Ambient + Emission + [Sigma] L_i [D * max(N*L, 0) + S*max(N*H,0)^s]
		*/
		finalcolor = ambient + emission;


		vec4 sigmacolor = vec4(0,0,0,1);
		for (int i = 0; i < numused; i++) 
		{
			//vec4 light_p = vec4(0,0,0,1);
			//vec4 light_c = vec4(0,0,0,1);
			vec4 light_p = lightposn[i];
			vec4 light_c = lightcolor[i];
			/*
			if (i == 0) {
				//light_p = vec4(0.6, 0, 0.1, 0);
				//light_c = vec4(1, 0.5, 0, 1);
				//light_p = vec4(0,2,10,0);
				//light_c = vec4(0.6,0.3,0.0,1.0);
			} else {
				light_p = vec4(0, -0.6, 0.1, 1);
				light_c = vec4(0.5, 0.5, 1, 1);
			}
			*/

			vec3 direction = vec3(0,0,1);

			//if light_p.w == 0, light is directional.
			if (light_p.w == 0)	
			{
				direction = normalize(light_p.xyz);
			} 
			else // ELSE, light is a point
			{
				vec3 position = light_p.xyz/light_p.w;
				direction = normalize(position - mypos3);
			}

			vec3 half_v = normalize(direction + eyedirn3);

			vec4 newcolor = ComputeLight(direction, light_c, normal3, half_v,
								diffuse, specular, shininess);

			//finalcolor = finalcolor + newcolor;
			sigmacolor = sigmacolor + newcolor;
		}
		
		finalcolor = finalcolor + sigmacolor;

        // Color all pixels black for now, remove this in your implementation!
        //finalcolor = vec4(0.0f, 0.0f, 0.0f, 1.0f); 

        fragColor = finalcolor; 
    } else {
        fragColor = vec4(color, 1.0f); 
    }
}
