// Transform.cpp: implementation of the Transform class.

// Note: when you construct a matrix using mat4() or mat3(), it will be COLUMN-MAJOR
// Keep this in mind in readfile.cpp and display.cpp
// See FAQ for more details or if you're having problems.

#include "Transform.h"

// Helper rotation function.  Please implement this.  
mat3 Transform::rotate(const float degrees, const vec3& axis) 
{
	mat3 parallel; mat3 ortho; mat3 dual;
	vec3 n_axis = glm::normalize(axis);
	float rd = glm::radians(degrees);

	float xy; float xz; float yz;

	// set parallel matrix to I3 values
	parallel = mat3(1, 0, 0, 0, 1, 0, 0, 0, 1);
	// set ortho matrix to formula values (w/ x,y,z values from axis)
	xy = n_axis.x * n_axis.y;
	xz = n_axis.x * n_axis.z;
	yz = n_axis.y * n_axis.z;
	ortho = mat3(n_axis.x * n_axis.x, xy, xz,
		xy, n_axis.y * n_axis.y, yz,
		xz, yz, n_axis.z * n_axis.z);
	// set dual matrix to formula values (w/ x,y,z values from axis)
	dual = glm::transpose(mat3(0, -n_axis.z, n_axis.y,
		n_axis.z, 0, -n_axis.x,
		-n_axis.y, n_axis.x, 0));

	// Axis-Angle formula
	mat3 result = parallel * float(cos(rd));
	result += float(1 - cos(rd)) * ortho;
	result += float(sin(rd)) * dual;

	// You will change this return call
	return result;
}

void Transform::left(float degrees, vec3& eye, vec3& up) 
{
	// 3D rotation code here
	mat3 left = rotate(degrees, -up);
	//printMatrix(left);
	eye = eye * left;
}

void Transform::up(float degrees, vec3& eye, vec3& up) 
{
	//printf("Coordinates: %.2f, %.2f, %.2f; distance: %.2f\n",
	//eye.x, eye.y, eye.z, sqrt(pow(eye.x, 2) + pow(eye.y, 2) + pow(eye.z, 2)));
	vec3 rightAxis = glm::cross(up, eye);
	//printf("leftAxis: %.2f, %.2f, %.2f\n",
	//leftAxis[0], leftAxis[1], leftAxis[2]);
	mat3 upRot = rotate(degrees, rightAxis);
	//printMatrix(upRot);
	up = up * upRot;
	mat3 leftRot = rotate(degrees, rightAxis);
	//printMatrix(leftRot);
	eye = eye * leftRot;
}

mat4 Transform::lookAt(const vec3 &eye, const vec3 &center, const vec3 &up) 
{
	// create vector w of new coord frame
	vec3 w = glm::normalize(eye);
	// create vector u of new coord frame
	vec3 u = glm::normalize(glm::cross(up, w));
	// create vector v of new coord frame
	vec3 v = glm::cross(w, u);

	mat4 rotMatrix = glm::transpose(
		mat4(
			u.x, u.y, u.z, 0,
			v.x, v.y, v.z, 0,
			w.x, w.y, w.z, 0,
			0, 0, 0, 1
		)
	);
	mat4 eyeMatrix = glm::transpose(
		mat4(
			1, 0, 0, -eye.x,
			0, 1, 0, -eye.y,
			0, 0, 1, -eye.z,
			0, 0, 0, 1
		)
	);

	// You will change this return call
	return rotMatrix * eyeMatrix;
}

mat4 Transform::perspective(float fovy, float aspect, float zNear, float zFar)
{
    mat4 ret;
	/* REMINDER: INPUTS ARE DEGREES, BUT ALL THESE FUNCTIONS NEED RADIANS*/
	float fovy_rad = glm::radians(fovy);
	float theta = fovy_rad / 2;
	float depth = (cos(theta)/sin(theta));
	float A = (-1) * ((zFar + zNear) / (zFar - zNear));
	float B = (-2 * zFar * zNear) / (zFar - zNear);
	mat4 perspectiveMatrix = glm::transpose(
		mat4(
			(depth / aspect), 0, 0, 0,
			0, depth, 0, 0,
			0, 0, A, B,
			0, 0, -1, 0
		)
	);
    // YOUR CODE FOR HW2 HERE
    // New, to implement the perspective transform as well.  
    return perspectiveMatrix;
}

mat4 Transform::scale(const float &sx, const float &sy, const float &sz) 
{
    mat4 ret;
	mat4 scaleMatrix = glm::transpose(
		mat4(
			sx, 0, 0, 0,
			0, sy, 0, 0,
			0, 0, sz, 0,
			0, 0, 0, 1
		)
	);
    return scaleMatrix;
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz) 
{
    mat4 ret;
	mat4 translateMatrix = glm::transpose(
		mat4(
			1, 0, 0, tx,
			0, 1, 0, ty,
			0, 0, 1, tz,
			0, 0, 0, 1
		)
	);
    return translateMatrix;
}

// To normalize the up direction and construct a coordinate frame.  
// As discussed in the lecture.  May be relevant to create a properly 
// orthogonal and normalized up. 
// This function is provided as a helper, in case you want to use it. 
// Using this function (in readfile.cpp or display.cpp) is optional.  

vec3 Transform::upvector(const vec3 &up, const vec3 & zvec) 
{
    vec3 x = glm::cross(up,zvec); 
    vec3 y = glm::cross(zvec,x); 
    vec3 ret = glm::normalize(y); 
    return ret; 
}


Transform::Transform()
{

}

Transform::~Transform()
{

}
