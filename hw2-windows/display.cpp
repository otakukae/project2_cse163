/*****************************************************************************/
/* This is the program skeleton for homework 2 in CSE167 by Ravi Ramamoorthi */
/* Extends HW 1 to deal with shading, more transforms and multiple objects   */
/*****************************************************************************/

// This file is display.cpp.  It includes the skeleton for the display routine

// Basic includes to get this file to work.  
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <deque>
#include <stack>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Transform.h"
#include "Geometry.h"

using namespace std; 
#include "variables.h"
#include "readfile.h"

// New helper transformation function to transform vector by modelview 
// May be better done using newer glm functionality.
// Provided for your convenience.  Use is optional.  
// Some of you may want to use the more modern routines in readfile.cpp 
// that can also be used.  
void transformvec (const GLfloat input[4], GLfloat output[4]) 
{
	glm::vec4 inputvec(input[0], input[1], input[2], input[3]);

	glm::vec4 outputvec = modelview * inputvec;

	output[0] = outputvec[0];

	output[1] = outputvec[1];

	output[2] = outputvec[2];

	output[3] = outputvec[3];
}

void display() 
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glClearColor(0, 0, 1, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Set up the camera view

  // Either use the built-in lookAt function or the lookAt implemented by the user.
  if (useGlu) {
    modelview = glm::lookAt(eye,center,up); 
  } else {
    modelview = Transform::lookAt(eye,center,up); 
  }

  glUniformMatrix4fv(modelviewPos, 1, GL_FALSE, &modelview[0][0]);

  glUniform1i(enablelighting, true);
  float def[4] = { 0.0, 0.0, 0.0, 1.0 };
  float defAmb[4] = { 0.2, 0.2, 0.2, 1.0 };
  float defDif[4] = { 0.8, 0.8, 0.8, 1.0 };
  float defSpe[4] = { 0.0, 0.0, 0.0, 1.0 };
  glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, def);
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, defAmb);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, defDif);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defSpe);

  float colorTotal[4] = { .2, .2, .2, 1.0 };
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, &colorTotal[0]);
  float pos2[4] = { 0, 3, 0, 1 };
  glLightfv(GL_LIGHT1, GL_POSITION, &pos2[0]);
  float color2[4] = { 1, 0, 1, 1 };
  glLightfv(GL_LIGHT1, GL_AMBIENT_AND_DIFFUSE, color2);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, &color2[0]);
  glLightfv(GL_LIGHT1, GL_SPECULAR, &color2[0]);

  // Lights are transformed by current modelview matrix. 
  // The shader can't do this globally. 
  // So we need to do so manually.  

  //cerr << "eye: (" << eye[0] << ", " << eye[1] << ", " << eye[2] << ")\n";
  //cerr << "center: (" << center[0] << ", " << center[1] << ", " << center[2] << ")\n";
	  float sinxAmbDif[4] = { 1.0, 0.3, 1.0, 1.0 };
	  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, sinxAmbDif);
	  float sinxSpe[4] = { 1.0, 1.0, 1.0, 1.0 };
	  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, sinxSpe);
	  float sinxShine = 100.0f;
	  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, sinxShine);
  for (int i = 0; i < num_vertices163; i++) {
	  drawTriangle(i);
	  //cerr << i << ": (" << vertexarray[i][0] << ", " << vertexarray[i][1] << ", " << vertexarray[i][2] << ")\n";
  }


  if (numused) {
    glUniform1i(enablelighting,true);

    // YOUR CODE FOR HW 2 HERE.  
    // You need to pass the light positions and colors to the shader. 
    // glUniform4fv() and similar functions will be useful. See FAQ for help with these functions.
    // The lightransf[] array in variables.h and transformvec() might also be useful here.
    // Remember that light positions must be transformed by modelview.
	///*
	for (int i = 0; i < numused; i++) {
		transformvec(&lightposn[i * 4], &lightransf[i * 4]);


		//glm::vec4 input = vec4(1.0);
		for (int j = (i*4); j < (i*4) + 4; j++) {
			//input[j - (i * 4)] = lightposn[j];

			//cout << j << "\tlightposn: " << lightposn[j]
				//<< "\tlighttransf: " << lightransf[j] << endl;
		}
		//cout << endl;
	}
	//*/
	//vec4 light_p = vec4(lightposn[0], lightposn[1], lightposn[2], lightposn[3]);
	/*
	for (int k = 0; k < numLights * 4; k+=4) {
		cout << "light: " << lightposn[k] << ", "
			<< lightposn[k+1] << ", " << lightposn[k+2]
			<< ", " << lightposn[k+3] << endl;
	}
	*/
	glUniform4fv(lightpos, numused, &lightransf[0]);
	glUniform4fv(lightcol, numused, &lightcolor[0]);
	glUniform1i(numusedcol, numused);

  } else {
    glUniform1i(enablelighting,false); 
  }

  // Transformations for objects, involving translation and scaling 
  mat4 sc(1.0) , tr(1.0), transf(1.0); 
  sc = Transform::scale(sx,sy,1.0); 
  tr = Transform::translate(tx,ty,0.0); 

  // YOUR CODE FOR HW 2 HERE. (done) 
  // You need to use scale, translate and modelview to 
  // set up the net transformation matrix for the objects.  
  // Account for GLM issues, matrix order (!!), etc.  
  transf = modelview * tr * sc;
  //transf = sc * tr * modelview;

  // The object draw functions will need to further modify the top of the stack,

  // so assign whatever transformation matrix you intend to work with to modelview

  // rather than use a uniform variable for that.
  modelview = transf;
  
  for (int i = 0 ; i < numobjects ; i++) {
    object* obj = &(objects[i]); // Grabs an object struct.

    // YOUR CODE FOR HW 2 HERE. 
    // Set up the object transformations 
    // And pass in the appropriate material properties
    // Again glUniform() related functions will be useful
	/*
	mat4 test_trans = obj->transform;
		printf("obj->transform: %.2f, %.2f, %.2f, %.2f\n",
		test_trans[0], test_trans[1], test_trans[2], test_trans[3]);
	*/
	// object transformation
	modelview = transf * (obj->transform);
	// passing in material properties
	glUniform4fv(ambientcol, 1, obj->ambient);
	glUniform4fv(diffusecol, 1, obj->diffuse);
	glUniform4fv(specularcol, 1, obj->specular);
	glUniform1f(shininesscol, obj->shininess);

	
    // Actually draw the object
    // We provide the actual drawing functions for you.  
    // Remember that obj->type is notation for accessing struct fields
    if (obj->type == cube) {
      solidCube(obj->size); 
    }
    else if (obj->type == sphere) {
      const int tessel = 20; 
      solidSphere(obj->size, tessel, tessel); 
    }
    else if (obj->type == teapot) {
      solidTeapot(obj->size); 
    }
	
  }
  
  glutSwapBuffers();
}
