Assignment 2: Meshes
Kevin Luu and Vincent Huang
To operate this program simply open it up in Visual Studio and change the properties of the solution to 
debug for a specific OFF file. 
q - edge collapse for an edge, denoted by 2 vertices
w - edge reconstruction for an edge, denoted by 2 vertices
arrow keys - move the figure around
z - zoom in
x - zoom out
m - progressive mesh
